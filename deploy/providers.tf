provider "aws" {
  region = var.provider_region
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.50.0"
    }
  }

  backend "s3" {
    bucket         = "tech.inlabs.recipe"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "tech.inlabs.recipe.tf.state.lock"
  }

  required_version = "1.0.5"
}
