locals {
  prefix = "${var.project_name}-${terraform.workspace}"
  common_tags = {
    ManagedBy   = "terraform"
    Environment = terraform.workspace
    Project     = var.project_name
  }
}
