output "db_host" {
  value       = aws_db_instance.main.address
  description = "Database host"
}

output "bastion_host" {
  value = {
    dns = aws_instance.bastion.public_dns,
    ip  = aws_instance.bastion.public_ip
  }
  description = "Bastion host"
}

output "api_endpoint" {
  value = aws_route53_record.app.fqdn
}

data "aws_ecr_repository" "recipe" {
  name = "recepi-api"
}

output "create_super_user" {
  value = {
    login             = "$(aws ecr get-login --no-include-email --region ${var.provider_region})"
    create_super_user = "docker run -it -e DB_HOST=${aws_db_instance.main.address} -e DB_NAME=${aws_db_instance.main.name} -e DB_USER=${aws_db_instance.main.username} -e DB_PASS=USE_YOUR_PASSWORD ${data.aws_ecr_repository.recipe.repository_url}:latest sh -c 'python manage.py wait_for_db && python manage.py createsuperuser'"
  }
  description = "Steps to create a new superuser"
}
