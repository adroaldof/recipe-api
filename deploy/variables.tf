variable "project_name" {
  type        = string
  description = "Project name to be used as prefix"
}

variable "provider_region" {
  type        = string
  description = "Infrastructure region placement"
}

variable "instance_type" {
  type        = string
  description = "EC2 instance type"
  default     = "t2.micro"
}

variable "tags" {
  type        = map(string)
  description = "Tags overrides"
  default     = {}
}

variable "vpc_cidr_block" {
  type        = string
  description = "VPC CIDR Block"
}

variable "subnet_public_a_cidr_block" {
  type        = string
  description = "VPC CIDR Block"
}

variable "subnet_public_b_cidr_block" {
  type        = string
  description = "VPC CIDR Block"
}

variable "subnet_private_a_cidr_block" {
  type        = string
  description = "VPC CIDR Block"
}

variable "subnet_private_b_cidr_block" {
  type        = string
  description = "VPC CIDR Block"
}

variable "db_username" {
  type        = string
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  type        = string
  description = "Password for the RDS Postgres instance"
}

variable "bastion_key_name" {
  type        = string
  description = "Valid AWS Key Pair name"
  default     = "inlabs-tech-api-key-adroaldo.andrade.tech"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "045160975173.dkr.ecr.us-east-1.amazonaws.com/recepi-api:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "045160975173.dkr.ecr.us-east-1.amazonaws.com/recipe-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "inlabs.technology"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
