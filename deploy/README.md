# Terraform

https://www.terraform.io/docs/glossary.html

First setup your credentials in the `.env` (follow the `.env.sample`)

## Docker Compose Run

```bash
docker-compose run terraform
```

## Docker Run

```bash
docker run -it -v $PWD:/app --env-file=.env -w /app --entrypoint "" hashicorp/terraform:1.0.2 sh
```

---

Enjoy it :wink:
