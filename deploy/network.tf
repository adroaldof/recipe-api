resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    {
      Name = "${local.prefix}-vpc"
    },
    local.common_tags
  )
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name = "${local.prefix}-vpc"
    },
    local.common_tags
  )
}

# Public Subnets - Inbound/Outbound Internet access

resource "aws_subnet" "public_a" {
  cidr_block              = var.subnet_public_a_cidr_block
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main.id
  availability_zone       = "${data.aws_region.current.name}a"

  tags = merge(
    {
      Name = "${local.prefix}-subnet-public-a"
    },
    local.common_tags
  )
}

resource "aws_route_table" "public_a" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name = "${local.prefix}-route-table-public-a"
    },
    local.common_tags
  )
}

resource "aws_route_table_association" "public_a" {
  subnet_id      = aws_subnet.public_a.id
  route_table_id = aws_route_table.public_a.id
}

resource "aws_route" "public_internet_access_a" {
  route_table_id         = aws_route_table.public_a.id
  gateway_id             = aws_internet_gateway.main.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_eip" "public_a" {
  vpc = true

  tags = merge(
    {
      Name = "${local.prefix}-elastic-ip-public-a"
    },
    local.common_tags
  )
}

resource "aws_nat_gateway" "public_a" {
  allocation_id = aws_eip.public_a.id
  subnet_id     = aws_subnet.public_a.id

  tags = merge(
    {
      Name = "${local.prefix}-nat-gateway-public-a"
    },
    local.common_tags
  )
}

resource "aws_subnet" "public_b" {
  cidr_block              = var.subnet_public_b_cidr_block
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main.id
  availability_zone       = "${data.aws_region.current.name}b"

  tags = merge(
    {
      Name = "${local.prefix}-subnet-public-b"
    },
    local.common_tags
  )
}

resource "aws_route_table" "public_b" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name = "${local.prefix}-route-table-public-b"
    },
    local.common_tags
  )
}

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.public_b.id
  route_table_id = aws_route_table.public_b.id
}

resource "aws_route" "public_internet_access_b" {
  route_table_id         = aws_route_table.public_b.id
  gateway_id             = aws_internet_gateway.main.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_eip" "public_b" {
  vpc = true

  tags = merge(
    {
      Name = "${local.prefix}-elastic-ip-public-b"
    },
    local.common_tags
  )
}

resource "aws_nat_gateway" "public_b" {
  allocation_id = aws_eip.public_b.id
  subnet_id     = aws_subnet.public_b.id

  tags = merge(
    {
      Name = "${local.prefix}-nat-gateway-public-b"
    },
    local.common_tags
  )
}

# Private Subnets - Outbound internet access only

resource "aws_subnet" "private_a" {
  cidr_block        = var.subnet_private_a_cidr_block
  vpc_id            = aws_vpc.main.id
  availability_zone = "${data.aws_region.current.name}a"

  tags = merge(
    {
      Name = "${local.prefix}-subnet-private-a"
    },
    local.common_tags
  )
}

resource "aws_route_table" "private_a" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name = "${local.prefix}-route-table-private-a"
    },
    local.common_tags
  )
}

resource "aws_route_table_association" "private_a" {
  subnet_id      = aws_subnet.private_a.id
  route_table_id = aws_route_table.private_a.id
}

resource "aws_route" "private_a_internet_out" {
  route_table_id         = aws_route_table.private_a.id
  nat_gateway_id         = aws_nat_gateway.public_a.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_subnet" "private_b" {
  cidr_block        = var.subnet_private_b_cidr_block
  vpc_id            = aws_vpc.main.id
  availability_zone = "${data.aws_region.current.name}b"

  tags = merge(
    {
      Name = "${local.prefix}-subnet-private-b"
    },
    local.common_tags
  )
}

resource "aws_route_table" "private_b" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    {
      Name = "${local.prefix}-route-table-private-b"
    },
    local.common_tags
  )
}

resource "aws_route_table_association" "private_b" {
  subnet_id      = aws_subnet.private_b.id
  route_table_id = aws_route_table.private_b.id
}

resource "aws_route" "private_b_internet_out" {
  route_table_id         = aws_route_table.private_b.id
  nat_gateway_id         = aws_nat_gateway.public_b.id
  destination_cidr_block = "0.0.0.0/0"
}
